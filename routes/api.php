<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'auth', 'namespace' => 'App\Http\Controllers\Auth'], function () {
    Route::post('register', 'AuthController@register'); /* SUCCES */
    Route::get('{user}/profile', 'AuthController@profile'); # SUCCESS

});

Route::group(['namespace' => 'App\Http\Controllers'], function () {
    Route::get('todolists', 'TodolistController@index'); # SUCCESS
    Route::group(['prefix' => 'users'], function () {
    Route::get('{user}/todolists', 'TodolistController@show'); # SUCCESS
    Route::post('{user}/todolists/', 'TodolistController@store'); # SUCCESS
    Route::patch('{user}/todolists/items', 'TodolistController@update');
    Route::post('{user}/todolists/items', 'TodoListController@addItemInTodoList'); # SUCCESS
    Route::delete('{user}/todolists/items', 'TodolistController@destroy');
    });
});

