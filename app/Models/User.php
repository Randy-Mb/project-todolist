<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Exception;

/**
 * Class User
 *
 * @property int $id
 * @property string $email
 *  @property Carbon $email_verified_at
 * @property string $firstname
 * @property string $lastname
 * @property int $age
 * @property Carbon $birthday
 * @property string $password
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @package App\Models
 */

class User extends Model
{
	protected $table = 'users';

	protected $dates = [
		'email_verified_at'
	];

    protected $casts = [
		'age' => 'int'
	];

	protected $hidden = [
		'password'
	];

	protected $fillable = [
        'firstname',
        'lastname',
		'email',
        'birthday',
		'email_verified_at',
		'password'
	];

    public function todoList()
    {
        return $this->hasOne(TodoList::class);
    }

    public function isValid()
    {
        return !empty($this->email)
            && filter_var($this->email, FILTER_VALIDATE_EMAIL)
            && !empty($this->firstname)
            && !empty($this->lastname)
            && !empty($this->password)
            && strlen($this->password) >= 8
            && strlen($this->password) <= 40
            && !empty($this->birthday)
            && Carbon::now()->subYears(13)->isAfter($this->birthday);
    }

    public function getName()
    {
        return $this->firstname;
    }
}
