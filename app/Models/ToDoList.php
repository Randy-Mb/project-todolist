<?php

namespace App\Models;

use Exception;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class TodoList
 *
 * @property int $id
 * @property string $name
 * @property int $id_user
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @package App\Models
 */
class TodoList extends Model
{

	protected $casts = [
        'created_at' => 'datetime',
        'updated_at' => 'datetime'
	];

	protected $fillable = [
		'name',
		'description'
    ];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'todolists';

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function items()
    {
        return $this->hasMany(Item::class)->orderBy('created_at', 'desc');
    }

    public function isValid(): bool
    {
        return !empty($this->name)
            && strlen($this->name) <= 255
            && (is_null($this->description) || strlen($this->description) <= 255);
    }

    /**
     * @param Item $item
     * @return Item
     * @throws Exception
     */
    public function canAddAnItem(Item $item): Item
    {
        if (is_null($item) || !$item->isValid()) {
            throw new Exception('Item vide-invalide');
        }

        if (is_null($this->user) || !$this->user->isValid()) {
            dd($this->user);
            throw new Exception('User vide-invalide');
        }

        if ($this->itemsCount() >= 10) {
            throw new Exception('TodoList pleine');
        }

        $lastItem = $this->getLastItem();
        if (!is_null($this->getLastItem()) && Carbon::now()->subMinutes(30)->isBefore($lastItem->created_at)) {
            throw new Exception('Patienter 30 min entre lajout de chaque item');
        }

        return $item;
    }

    protected function getLastItem(): ?Item
    {
        return $this->items->first();
    }

    protected function itemsCount()
    {
        return sizeof($this->items()->get());
    }
}