<?php

namespace App\Http\Controllers;

use App\Models\TodoList;
use App\Models\User;
use App\Models\Item;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class todoListController extends Controller
{


    /**
     * Display a listing of the resource.
     *
     * @return TodoList[]
     */
    public function index()
    {
        return TodoList::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @param User $user
     * @return JsonResponse|object
     * @throws Exception
     */
    public function store(Request $request, User $user)
    {
        /*if(TodoList::create($request ->all())){
            return response()->json([
                'sucess' => "Todolist crée avec success"
            ],200);
        }*/

        $request->validate([
            'name' => 'required|string|max:255',
            'description' => 'nullable'
        ]);

        $name = $request->name;
        $desc = $request->description;
        //dd($name);
        if (!$this->createTodo($user, $name, $desc)) {
            throw new Exception("Erreur pendant la creation de la todolist du User");
        }

        return response()
            ->json(['todolist' => $this->getTodoListWithItems($user)])
            ->setStatusCode(201);
    }

    /********************************************************************************* */

    /**
     * Display the specified resource.
     *
     * @param User $user
     * @return JsonResponse
     */
    public function show(User $user): JsonResponse
    {
        return response()->json(['todolist' => User::whereId($user->id)->with('todoList.items')->first()->todoList]);
    }

    /********************************************************************************* */

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param TodoList $todoList
     * @return JsonResponse
     */
    public function update(Request $request, Todolist $todoList)
    {
        if($todoList->update($request->all())){
            return response()->json([
                'sucess' => 'Todolist modifiée avec succès'
            ],200);
        }
    }

    /********************************************************************************* */

    /**
     * Remove the specified resource from storage.
     *
     * @param TodoList $todoList
     * @return void
     * @throws Exception
     */
    public function destroy(TodoList $todoList)
    {
        $todoList->delete();
    }

    /********************************************************************************* */

    /**
     * @param Request $request
     * @param User $user
     * @return JsonResponse
     * @throws Exception
     */
    protected function addItemInTodoList(Request $request, User $user): JsonResponse
    {
        $name = $request->name;
        $content = $request->content;

        if (!$this->addAnItem($user, $name, $content)) {
            throw new Exception("Impossible d'ajouter l'item dans la Todolist");
        }

        return response()
            ->json(['todolist' => $this->getTodoListWithItems($user)])
            ->setStatusCode(201);
    }

    /*********************************************************************************
     * @param User $user
     * @param string $name
     * @param string|null $description
     * @return bool
     */
    protected function createTodo(User $user, string $name, ?string $description): bool
    {
        if (is_null($user) || !$user->isValid()) {
            return false;
        }

        $todoList = TodoList::make([
            'name' => $name,
            'description' => $description
        ]);

        $user->todoList()->save($todoList);
        return true;
    }

    /********************************************************************************* */

    public function addAnItem(User $user, string $name, string $content): bool
    {
        if (!$user->isValid()) {
            return false;
        } elseif (is_null($user->todoList)) {
            return false;
        }

        foreach ($user->todoList->items as $item) {
            if ($item->name === $name) {
                return false;
            }
        }

        $item = Item::make([
            'name' => $name,
            'content' => $content
        ]);

        if ($user->todoList->canAddAnItem($item)) {
            $user->todoList->items()->save($item);
             $user->todoList->load('items');

            return true;
        }

        return false;
    }

    /********************************************************************************* */

    public function getTodoListWithItems(User $user): TodoList
    {
        return User::whereId($user->id)->with('todoList.items')->first()->todoList;
    }
}
