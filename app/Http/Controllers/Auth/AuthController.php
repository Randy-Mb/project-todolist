<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param Request $request
     * @return JsonResponse|object
     */
    public function register(Request $request)
    {
        $user = new User();
        $user->fill($request->all());
        $user->save();

        /* $request->validate([
            'firstname' => 'required',
            'lastname' => 'required',
            'email' => 'required',
            'birthday' => 'required',
            'password' => 'required|min:4'
        ]);

        $user = User::create($request->only($request->all())); */

        return response()
                ->json([
                    'user' => 'User created successfully',
                    'todolists' => $this->getUserTodoList($user)
                ])
                ->setStatusCode(201);
    }

    public function profile(User $user)
    {
        return new UserResource($user);
    }

    public function getUserTodoList(User $user): User
    {
        return User::whereId($user->id)->with('todoList.items')->first();
    }
}
