<?php

namespace Tests\Unit;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Str;
use PHPUnit\Framework\TestCase;

class UserTest extends TestCase
{
    private $user;

    protected function setUp(): void
    {
        parent::setUp();

        $this->user = new User([
            'email' => 'ahbahoui@ahabahoui.fr',
            'firstname' => 'Randy',
            'password' => 'imthepassword',
            'birthday' => Carbon::now()->subDecades(21)->subMonths(7)->subDays(4)->toDateString()
        ]);
    }

    public function testIsValidUser()
    {
        $this->assertTrue($this->user->isValid());
    }

    public function testIsNotValidEmail()
    {
        $this->user->email = 'blablabla';
        $this->assertFalse($this->user->isValid());
    }

    public function testIsNotValidFirstName()
    {
        $this->user->firstname = '';
        $this->assertFalse($this->user->isValid());
    }

    public function testIsNotValidLastNameNull()
    {
        $this->user->lastname = null;
        $this->assertFalse($this->user->isValid());
    }

    public function testIsNotValidLongPassword()
    {
        $this->user->password = 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa';
        $this->assertFalse($this->user->isValid());
    }

    public function testIsNotValidShortPassword()
    {
        $this->user->password = 'abcd';
        $this->assertFalse($this->user->isValid());
    }

    public function testIsNotValidAge()
    {
        $this->user->birthday = Carbon::now()->subDecade()->toDateString();
        $this->assertFalse($this->user->isValid());
    }

}
