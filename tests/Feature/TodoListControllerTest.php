<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\DatabaseMigrations;


class TodoListControllerTest extends TestCase
{
    use DatabaseMigrations;

    private $user;

    protected function setUp(): void
    {
        parent::setUp();

        $this->user = new User([
            'email' => 'ahbahoui@ahabahoui.fr',
            'firstname' => 'Randy',
            'password' => 'imthepassword',
            'birthday' => Carbon::now()->subDecades(21)->subMonths(7)->subDays(4)->toDateString()
        ]);
    }

    public function testAddItemInTodoList() {

        $response = $this->postJson('/api/users/' . strval($this->user->id) . '/todolists', [
            'name' => 'Un petit nom de todolist',
            'description' => 'une petite description de todolist'
        ]);

        $response->assertCreated();


        $name = 'Coucou';
        $content = 'Coucou c\'est bien moi hehehe!';

        $response = $this->postJson('/api/users/' . strval($this->user->id) . '/todolists/items', [
            'name' => $name,
            'content' => $content
        ]);
    }

    /**
     * @test
     */
    public function createTodoListTest()
    {
        $response = $this->postJson('/api/users/' . $this->user->id . '/todolists', [
            'name' => 'Projet',
            'description' => 'Faire des tests unitaires, fonctionnels et d\'integration'
        ]);

        $response->assertCreated();

        $this->assertDatabaseHas('todolist', [
            'id' => $this->user->id,
            'name' => 'Projet',
            'description' => 'Faire des tests unitaires, fonctionnels et d\'integration'
        ]);
    }
}
