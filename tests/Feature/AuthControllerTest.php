<?php

namespace Tests\Feature;

use App\Http\Controllers\UserController;
use Carbon\Factory as CarbonFactory;
use Faker\Factory;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class AuthControllerTest extends TestCase
{
    use RefreshDatabase;
    
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testExample()
    {
        $response = $this->get('/');

        $response->assertStatus(201);
    }

    // public function testRequiredFieldsForRegistration()
    // {
    //     $this->json('POST', 'api/register', ['Accept' => 'application/json'])
    //         ->assertStatus(422)
    //         ->assertJson([
    //             "message" => "The given data was invalid.",
    //             "errors" => [
    //                 "name" => ["The name field is required."],
    //                 "email" => ["The email field is required."],
    //                 "password" => ["The password field is required."],
    //             ]
    //         ]);
    // }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testUserRegister()
    {
        $user =[
            "firstname" =>  "Andrew", 
            "lastname" =>  "mondor" , 
            "email" => "andrew@gmail.com",
            "password" => "password" ,
            "birthday"=> "23/06/1998", 
            
        ];

        $this->json('POST', 'api/auth/register', $user, ['Accept' => 'application/json'])
            ->assertStatus(201)
            ->assertJsonStructure([
                "user" => [
                    'firstname',
                    'lastname',
                    'email',
                    'birthday',
                    'created_at',
                    'updated_at',
                ],
                "access_token",
                "message"
            ]);
}

}
